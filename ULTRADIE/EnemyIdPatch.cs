using System.Management.Instrumentation;
using HarmonyLib;
using UnityEngine;

namespace UltraDie {
    [HarmonyPatch(typeof(EnemyIdentifier), "Start")]
    public class EnemyIdPatch {
        [HarmonyPrefix]
        public static void Prefix(EnemyIdentifier __instance) {
            MaxHpHolder mhh = __instance.gameObject.AddComponent<MaxHpHolder>();
            mhh.id = __instance;
            mhh.maxHp = __instance.health;
            mhh.boss = __instance.GetComponent<BossIdentifier>() != null;
            
            Debug.Log($"Setting {mhh.maxHp} hp for {__instance.gameObject.name}. Is boss? {mhh.boss}");
        }
    }
}