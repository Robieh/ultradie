using UnityEngine;

namespace UltraDie {
    public class MaxHpHolder : MonoBehaviour {
        public EnemyIdentifier id;
        public float maxHp;
        public bool boss;
    }
}