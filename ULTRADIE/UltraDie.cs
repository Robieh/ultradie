﻿using System;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace UltraDie {
    [BepInPlugin(pluginGuid, pluginName, pluginVersion)]
    public class UltraDie : BaseUnityPlugin {
        public const string pluginGuid = "robiehh.uk.die";
        public const string pluginName = "ULTRADIE";
        public const string pluginVersion = "1.0";

        private int prevHp = 100;

        public ConfigEntry<float> healScale;
        public ConfigEntry<float> maxHpMultiplier;
        public ConfigEntry<float> range;
        public ConfigEntry<float> bossHealScale;
        public ConfigEntry<float> bossMaxHp;

        public UltraDie() {
            healScale = Config.Bind("ULTRADIE", "HealScale", 0.8f, new ConfigDescription("The amount of health received per damage"));
            maxHpMultiplier = Config.Bind("ULTRADIE", "MaxHealthScale", 1.5f, new ConfigDescription("The maximum amount of health an enemy can have from overheals (multiplier). Minimum value of 1."));
            range = Config.Bind("ULTRADIE", "Range", 10f,
                new ConfigDescription("The maximum range enemies can receive heal from."));
            bossHealScale = Config.Bind("ULTRADIE", "BossHealScale", 0.6f, new ConfigDescription("Same as HealScale, but for bosses"));
            bossMaxHp = Config.Bind("ULTRADIE", "BossMaxHealthScale", 1.1f, new ConfigDescription("Same as MaxHealScale, but for bosses"));
        }
        
        private void Awake() {
            var harmony = new Harmony(pluginGuid);
            harmony.PatchAll();
        }

        void Update() {
            if (MonoSingleton<NewMovement>.Instance == null) return;
            int hp = MonoSingleton<NewMovement>.Instance.hp;

            if (hp < prevHp) {
                int hpDiff = prevHp - hp;
                foreach (Collider c in Physics.OverlapSphere(MonoSingleton<NewMovement>.Instance.transform.position, range.Value)) {
                    if (c.CompareTag("Enemy") && c.TryGetComponent<MaxHpHolder>(out MaxHpHolder maxHp)) {
                        EnemyIdentifier identifier = maxHp.id;
                        Heal(identifier, hpDiff * (maxHp.boss ? bossHealScale.Value : healScale.Value), maxHp.maxHp * (maxHp.boss ? bossMaxHp.Value : maxHpMultiplier.Value));
                    }
                }
            }
            
            prevHp = hp;
        }

        public void Heal(EnemyIdentifier id, float health, float max) {
            if (id.enemyType == EnemyType.MaliciousFace) {
                id.spider.health = Mathf.Min(id.spider.health + health, max);
                id.ForceGetHealth();
                return;
            }
            switch (id.enemyClass) {
                case EnemyClass.Husk:
                    if (id.zombie != null) {
                        id.zombie.health = Mathf.Min(id.zombie.health + health, max);
                        id.ForceGetHealth();
                    }
                    break;
                case EnemyClass.Machine:
                    if (id.machine != null) {
                        id.machine.health = Mathf.Min(id.machine.health + health, max);
                        id.ForceGetHealth();
                    }
                    break;
                case EnemyClass.Demon:
                    if (id.statue != null) {
                        id.statue.health = Mathf.Min(id.statue.health + health, max);
                        id.ForceGetHealth();
                    }
                    break;
                default:
                    id.health = Mathf.Min(id.health + health, max);
                    break;
            }
        }
    }
}